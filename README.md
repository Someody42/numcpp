# NumCpp

**NumCpp** est une ébauche de librairie visant, entre autres, à simplifier la gestion des conteneurs en C++.

**NOTE IMPORTANTE (mars 2021) : Cette librairie n'est plus développée. Ce projet était un "proof of concept", réalisé avant l'introduction des `range` et des `views` en `C++20`, et m'a surtout permis de découvrir et utiliser les concepts avancés liés aux `templates`. Néanmoins, je tenais à rendre le projet public, tout d'abord par principe, mais surtout pour ce readme qui concentre les motivations et concepts clés, et qui pourrait, qui sait, servir de base à une version utilisable de la librairie.**

# Installation et usage

**NumCpp** est une librairie _header-only_, c'est à dire que l'entièreté de la librairie est comprise dans les fichiers `.hpp`. Pas de dépendances pour l'utilisateur, pas de librairies à lier au programme, tout se fait uniquement à l'aide de la directive `#include` ! \
Pour compiler un projet utilisant **NumCpp**, il est nécessaire d'installer la librairie **Boost.Hana** (installée avec **Boost**), elle aussi _header-only_. \
De plus, la librairie utilise les fonctionnalités et la librairie du standard `C++17`, et ne compilera pas sur une version antérieure. \
La librairie a été testée sous `GNU/Linux` avec les compilateurs `g++` et `clang++`.
L'entièreté de la librairie se trouve dans l'espace de noms `numcpp`, qu'il est conseillé d'utiliser en tant que `nc`, ce qui se fait grâce à l'instruction :

```c++
using namespace nc = numcpp;
```

# Fonctionnalités

**CETTE LIBRAIRIE EST UNE "PROOF OF CONCEPT", NE PAS L'UTILISER DANS UN CADRE PROFESSIONNEL/ACADÉMIQUE !!!**\
En revanche, n'hésitez pas à l'essayer, voire à l'utiliser dans des projets personnels, et à donner des retours !

Le développement s'est principalement concentré sur le module de gestion des conteneurs à _n_ dimensions, décrit dans le paragraphe suivant.

# Description du module de conteneurs à plusieurs dimensions

## Motivation

Si vous avez fait un peu d'informatique, vous êtes habitués à l'idée qu'un tableau de tableaux et un tableau à 2 dimensions sont intrinsèquement la même chose. Pour représenter une matrice, on verra par exemple souvent :  
```c++
template <typename T>
using matrix = std::vector<std::vector<T>>;
```
Si elle a le mérite de ne pas requérir d'apprentissage supplémentaire par rapport à l'usage de tableaux classiques, cette représentation naïve présente certains désavantages.
- Tout d'abord, elle mélange le contenant et le contenu, puisque la syntaxe d'accès aux éléments renvoie à son tour un conteneur. Le véritable contenu est alors relégué derrière de nombreuses couches de conteneurs. Une tâche aussi simple que multiplier tous les éléments par 2 devient embêtante et dépendante de la dimension du tableau. Jugez par vous même :
```c++
//Pour un tableau 1D
std::for_each(tab.begin(),tab.end(),[](auto& val){val*=2;});
//Pour un tableau 2D
std::for_each(tab.begin(),tab.end(),[](auto& sub){
    std::for_each(sub.begin(),sub.end(),[](auto& val){val*=2;});
});
```
- Ensuite, de manière encore plus importante, cette représentation à l'**énorme** désavantage de faire perdre toute symétrie au système. Pensez par exemple à un tableau à 2 dimensions représentant un plateau d'échecs (ou de squadro :D). Pour implémenter le jeu, il faudra être capable de chercher un élément donné dans une ligne ou dans une colonne. Voilà ce que ça donne :
```c++
//Recherche dans la i-ème ligne
std::find(plateau[i].begin(),plateau[i].end(),valeur);
//Recherche dans la i-ème colonne
std::find_if(plateau.begin(),plateau.end(),[valeur&](auto const& sub){
    return sub[i]==valeur;
});
```
Pour le même tableau, faire la même opération selon deux axes différents implique de tout repenser ! Mais il y a pire : la solution parfois conseillée pour pallier ce problème. On lira ainsi : "Il suffit de transposer/tourner/faire une symétrie du tableau pour se ramener au cas précédent". Et on verra alors des fonctions faisant cette transformation, _extrêmement coûteuse_, de manière répétée, et en la faisant toujours suivre de la transformation inverse. Le problème majeur, c'est que cela nécessite de changer la structure complète du conteneur juste pour changer notre manière de le voir ! C'est pour pallier à ce problème qu'a été développé le concept de **vues**.

## Idées de base : vues multidimensionnelles

L'objet central de la librairie est donc la vue multidimensionnelle, représentée par la classe _template_ suivante :
```c++
template <typename iter, size_t dim>
class nd_view;
```
Les paramètres associés peuvent paraître étranges :
- il faut spécifier la dimension : en plus d'être compliquée à implémenter, la déduction automatique de la dimension pose différents problèmes, tels que l'impossibilité de déclarer un tableau de tableau sans qu'il soit interprété comme tableau à 2 dimensions
- on ne spécifie pas le type des éléments, il sera automatiquement déduit. En revanche, on précise le type des itérateurs sur le conteneur "vu" par cette vue. En effet, une vue **ne possède pas ses éléments**, mais seulement des itérateurs sur ces éléments. En réalité, une vue n'est rien d'autre qu'une jolie interface autour d'une paire d'itérateurs, le début et la fin de la zone "observée".

À ce point, il faut signaler un point important : les vues de cette librairie sont distinctes et incompatibles avec les vues introduites en `C++20` dans le module `std::ranges` (bien que le concept soit similaire). Notamment, on ne retrouve pas le procédé d'évaluation paresseuse des vues.

Faisons un aperçu des fonctionnalités principales de cette classe :

### Accès aux éléments

Comme toute collection d'objets qui se respecte, la vue `nc::nd_view` permet d'accéder à ses éléments, ou plus exactement à ceux du conteneur observé. L'accès se fait de manière classique par l'opérateur `[]` et la méthode `at`. Cependant, pour différentes raisons liées à la conception de la librairie (j'y reviendrai plus loin), une bonne habitude à prendre est de grouper l'accès au éléments selon différents axes dans un seul appel de fonction. Dit plus simplement, on aimerait pouvoir écrire `view.at(a,b,c)` (ce qu'on peut faire) `view[a,b,c]` (ce qu'on ne peut pas faire). En effet, le langage `C++` impose un unique paramètre pour cet l'opérateur `[]`. On aura alors recours à la syntaxe peu élégante `view[std::tuple(a,b,c)]`. Si `a`,`b` et `c` sont tous des types entiers, cette syntaxe est strictement équivalente à `view[a][b][c]`. Cependant, cette dernière empêche des manières d'accès plus intéressantes de fonctionner. Pour apporter de l'homogénéité, il est donc fortement conseillé d'utiliser cette syntaxe universelle, ou alors son raccourci syntaxique : `view(a,b,c)`. Voilà finalement un résumé des syntaxes conseillées pour accéder aux éléments d'une vue :

|Syntaxe                  |Bound-checking        |
|-------------------------|----------------------|
|`view[std::tuple(a,b,c)]`|Non                   |
|`view(a,b,c)`            |Non                   |
|`view.at(a,b,c)`         |Oui                   |

Parlons maintenant des types des paramètres `a`,`b` et `c`. Évidemment, la manière la plus simple et classique et d'accéder aux éléments à l'aide de leur indice, ce qui a lieu lors de l'utilisation de types entiers. Plus exactement, le type utilisé doit être convertible en `size_t`, i.e en `unsigned long int`.

L'autre manière d'utiliser les opérateurs vus ci-dessus vient du... `python`. Malgré ma haine cordiale (ou pas) de ce langage, j'ai été bien obligé de reconnaître la simplicité d'utilisation de la notation `tableau[a:b,c]` qui désigne le tableau pour `i` allant de `a` à `b` des objets `tableau[i,c]`. Ne pouvant pas introduire de nouvelle syntaxe au `C++`, je ne pouvais pas rendre la syntaxe aussi élégante, mais on arrive néamoins au même résultat.

Le type de paramètre correspondant à ce mode d'accès est la structure `nc::seq_bounds` (pour _sequence bounds_). Cette structure est un _aggregate_, donc la syntaxe permettant d'obtenir la même chose qu'en `python` plus haut est :

```c++
view(nc::seq_bounds{a,b},c);
```

À noter qu'il est tout à fait possible de ne pas définir de maximum, ou encore de ne définir aucune des deux bornes (définir seulement le maximum n'est par contre pas possible). Le cas échéant, les bornes seront déterminées dynamiquement en fonction de la taille. Comme il arrive très souvent de ne pas vouloir spécifier les bornes, et pour alléger la syntaxe, on dispose de la variable globale `nc::all = nc::seq_bounds{};`. Accéder à tous les `a`-ièmes éléments d'une vue se fait alors par la syntaxe :

```c++
view(nc::all,a);
```

Nous avons maintenant tout ce qu'il faut pour comprendre l'intérêt de regrouper les indices en un même appel de fonction. Considérez le code :

```c++
view[nc::all][a];
```

Implicitement, le compilateur le lira ainsi :

```c++
(view[nc::all])[a]
```

Or, `view[nc:all]` est par définition **égal** à `view`. Ce code renvoie donc le `a`-ième sous-tableau plutôt que le tableau des `a`-ièmes éléments des sous-tableaux.

Voilà donc un petit tour des différentes méthodes d'accéder aux éléments d'une vue. Néanmoins, il reste à parler d'un point important, à savoir le type de retour de ces fonctions. Je vais donc finalement vous mettre face à une vérité surprenante : vous ne voulez pas le savoir. Voyons pourquoi avec le code ci-dessous.

```c++
namespace nc = numcpp;

int main(int argc, char const *argv[]) {
  std::vector<std::vector<int>> v({{0,1},
                                   {2,3}});
  nc::nd_view<decltype(v.begin()),2> view(v.begin(),v.end());
  std::cout<<view(nc::all,1)<<std::endl;
  return 0;
}
```

Voilà le type de retour de l'expression `view(nd::all,1)` :

```c++
nc::nd_view<
  nc::sequence_iterator_<
    nc::nd_view<
      std::vector<int>::iterator,
      2ul
    >,
    std::tuple<unsigned long, int>,
    unsigned long
  >,
  1ul
>
```

Vous en conviendrez, devoir écrire ce type ne serait-ce qu'une fois dans un programme annulerait tous les bénéfices que peuvent apporter cette librairie. Pour cette raison, comme avec de nombreuses librairies faisant grand usage de _templates_, il est fortement conseillé de laisser le langage travailler pour vous. On citera notamment :
- le mot clé `auto`, qui peut remplacer un type dans la plupart des situations où vous pourriez en avoir besoin, et qui laisse le compilateur faire l'inférence des types. À noter que celle ci est toujours faite statiquement et à la compilation, donc le typage reste fort dans le programme généré. Le principal défaut de ce mot clé est que, jusqu'en `C++20`, il n'est pas utilisable dans les paramètres de fonctions, et il faudra donc avoir recourt à des _templates_.
- le spécifieur `decltype(a)`, qui permet de générer le type de l'expression `a`. Le spécifieur `decltype(a)` peut alors être utilisé exactement de la même manière qu'un type. Un utilitaire pratique à utiliser avec `decltype` est le _template_ `std::declval<T>` qui permet d'utiliser `decltype` sans avoir à créer les objets nécessaires à l'écriture de l'expression. Par exemple, déterminer le type d'un itérateur sur un conteneur de type `C` peut se faire en utilisant la syntaxe :
```c++
decltype(std::declval<C>().begin());
```
Ici, l'appel à `std::declval<C>()` permet de ne pas avoir à créer d'objet `C` pour déterminer le type de retour de `C::begin`.
- Enfin, le dernier outil permettant de ne pas avoir à faire soi-même l'inférence des types est d'utiliser des _templates_. Il s'agit d'un outil extrêmement puissant, mais qui nécessite un apprentissage assez poussé. Dans l'idéal, j'aurais aimé que l'utilisation de la librairie ne nécessite pas la maîtrise de cet outil, mais il sera malheureusement nécessaire de l'utiliser dès que vous voudrez passer des vues en paramètres de fonctions. Plutôt qu'un tutoriel complet, je vous laisse avec un exemple d'une fonction renvoyant le premier élément d'une séquence :
```c++
template <typename I, size_t n>
auto front(nd_view<I,n> vue) {
    return vue[0];
}
```

Néanmoins, je ne peux pas vous dire comment utiliser la librairie si je ne vous donne pas quelques indications sur la manière dont vous pouvez les utiliser. 

### Construction et assignation

Vous commencez à avoir un petit aperçu de comment utiliser la librairie. Et pourtant, il vous manque un point important : comment crée-t-on une vue ?

Comme je l'ai dit plus haut, le concept de "vue" n'est rien d'autre qu'une abstraction de celui de "paire d'itérateurs", qui marquent le début et la fin de la séquence étudiée. Le premier constructeur de `nd_view` prends donc en paramètre deux itérateurs, représentant le début et la fin de la séquence étudiée. Son prototype est le suivant :
```c++
template <typename iter_t>
nd_view(iter_t first, iter_t last);
```
Le type `iter_t` doit alors être implicitement convertible vers le type d'itérateur passé en paramètre _template_.
Cependant, pour éviter de devoir écrire explicitement la paire d'itérateurs, on dispose également du constructeur :
```c++
template <typename range_t>
nd_view(range_t& range);
```
Ce dernier utilisera comme itérateurs ceux renvoyés par les méthodes `begin()` et `end()` de `range`. On a évidemment des déclinaisons pour les références constantes ou non-constantes et pour les références sur `rvalue`.

Après la construction, se pose maintenant la question de l'assignation. En effet, nous avons un choix de syntaxe à faire. Que doit faire l'instruction `vue = sequence;` ? Faut-il que `vue` devienne une vue sur la séquence `sequence`, ou que les éléments de `sequence` soit copiés à la place des éléments de `vue` ? Pour maintenir la cohérence avec les conteneurs classiques, et le `C++` ayant choisi une sémantique de copie pour l'affectation, le choix suivant a été fait :
- `vue = sequence;` entraînera la copie (avec vérification des bornes) des éléments de `sequence`.
- `vue >>= sequence;` fera de la variable `vue` une vue du conteneur `sequence`.

Voici un petit exemple pour illustrer cette différence importante :
```c++
std::vector<int> a({0,1,2,3});
std::vector<int> b({3,1,4,1,5});
std::vector<int> c({100,10,1});
nc::nd_view<decltype(a.begin()),1> vue(a);
std::cout<<vue<<std::endl; //Affiche {0,1,2,3}
vue = b;
std::cout<<vue<<std::endl; //Affiche {3,1,4,1}
std::cout<<a[0]<<std::endl; //Affiche 3
vue >>= c;
std::cout<<vue<<std::endl; //Affiche {100,10,1}
std::cout<<a[0]<<std::endl; //Affiche 3
```

Voilà donc le minimum pour utiliser les vues. Nous allons donc pouvoir attaquer les choses plus amusantes.

### Tout mettre à plat avec `flat_view`

Jusqu'ici, vous ne devez pas voir beaucoup de différence avec l'utilisation de conteneurs classiques, mis à part peut-être le concept de vue. En tout cas, rien qui concerne directement les tableaux à plusieurs dimensions. Comme je l'ai dit plus haut, un des problèmes de la vision classique des conteneurs multidimensionnels est qu'elle nécessite de traverser de nombreuses "couches" de conteneurs pour arriver enfin à ce qui est intéressant, à savoir le contenu. `numcpp` résout ce problème avec l'idée de vue "aplatie". La classe `nd_view` contient ainsi une méthode `flat_view` qui renvoie une vue sur les éléments de la vue, et en traversant pour vous les couches de conteneurs. Multiplier tous les éléments d'une vue par 2 s'obtient alors en écrivant :
```c++
auto vue_plate = vue.flat_view();
std::for_each(vue_plate.begin(),vue_plate.end(),[](auto& x){x*=2;});
```
Et ce code marche de la même manière que `vue` ait 1,2 ou 450 dimensions !
Si jamais vous souhaiteriez accéder directement aux itérateurs, vous serez sûrement intéressés par les méthodes `flat_begin` et `flat_end`. L'itérateur parcourt, dans l'ordre, `vue(0,...,0,0)`,`vue(0,...,0,1)`,`...`,`vue(0,...,1,0)`,`...`

### Suivre son chemin avec `sequence_view`

Cette fonctionnalité vous paraîtra sans doute moins attrayante, mais elle est simplifie l'écriture de fonctions plus intéressantes, comme celle que nous verrons ci-dessous. Surtout, elle permet une grande flexibilité dans la manipulation de vues. L'idée vient de la constatation suivante : accéder à une ligne ou une colonne d'une vue est désormais très simple (notamment grâce à l'utilisation de `nc::seq_bounds` comme indice). Cependant, travailler sur les éléments d'une diagonale nécessite encore de recourir à des boucles, car on ne dispose pas d'itérateur assez flexible pour se plier à de telles exigences.

Les itérateurs séquentiels sont là pour parer à ce problème. Encore une fois, un exemple vaudra mieux que mille explications. Le code suivant crée une vue sur la diagonale d'une matrice :

```c++
std::vector<std:vector<int>> v({{0,1,2},
                                {3,4,5},
                                {6,7,8}});
auto vue = nc::make_nd_view<2ul>(v);
auto diag = vue.sequence_view([](size_t n){return std::tuple(n,n);});
```

# ToDo

- améliorer l'opérateur d'égalité des sequence_iterator
