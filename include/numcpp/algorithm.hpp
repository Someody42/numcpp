#ifndef NUMCPP_ALGORITHM_HPP
#define NUMCPP_ALGORITHM_HPP

#include <algorithm>

/** BIBLIOTHÈQUE ALGORITHMIQUE
  * Ce fichier contient de nouveaux algorithmes, ainsi qu'une interface
  * en termes de range des algorithmes de la librairie standard
  */

namespace numcpp {
  template <typename I1, typename S1, typename I2>
  auto dot(I1 first, S1 last, I2 first2) {
    decltype((*first)*(*first2)) accumulator(0);
    for (; first != last;) {
      accumulator+= (*first)*(*first2);
      ++first; ++first2;
    }
    return accumulator;
  }

  template <typename I, typename S>
  auto mean(I first, S last) -> decltype((*first)*1.) {
    typename std::iterator_traits<I>::value_type accumulator(0);
    if (first==last) {
      return accumulator;
    }
    accumulator = *first++;
    size_t counter = 1;
    for (; first != last;) {
      accumulator+= (*first);
      ++first; ++counter;
    }
    return accumulator*(1./counter);
  }

  template <typename I, typename S, typename Compare>
  I it_max(I first, S last, Compare comp) {
    I ret;
    if (first==last) {
      return ret;
    }
    ret = first++;
    for (; first != last; ++first) {
      if(comp(*ret,*first)){
        ret = first;
      }
    }
    return ret;
  }

  template <typename I, typename S>
  I it_max(I first, S last) {
    I ret;
    if (first==last) {
      return ret;
    }
    ret = first++;
    for (; first != last; ++first) {
      if(*ret<*first){
        ret = first;
      }
    }
    return ret;
  }

  template <typename I, typename S, typename Compare>
  I it_min(I first, S last, Compare comp) {
    I ret;
    if (first==last) {
      return ret;
    }
    ret = first++;
    for (; first != last; ++first) {
      if(!comp(*ret,*first)){
        ret = first;
      }
    }
    return ret;
  }

  template <typename I, typename S>
  I it_min(I first, S last) {
    I ret;
    if (first==last) {
      return ret;
    }
    ret = first++;
    for (; first != last; ++first) {
      if(*ret>*first){
        ret = first;
      }
    }
    return ret;
  }

  namespace ranges{
    template <typename Rng, typename Rng2>
    auto dot(Rng range, Rng2 range2) {
      return dot(range.begin(),range.end(),range2.begin());
    }

    template <typename Rng>
    auto mean(Rng range) {
      return mean(range.begin(),range.end());
    }

    template <typename Rng, typename Compare>
    auto it_max(Rng range, Compare comp) {
      return ::numcpp::it_max(range.begin(),range.end(),comp);
    }

    template <typename Rng>
    auto it_max(Rng range) {
      return ::numcpp::it_max(range.begin(),range.end());
    }

    template <typename Rng, typename Compare>
    auto it_min(Rng range, Compare comp) {
      return ::numcpp::it_min(range.begin(),range.end(),comp);
    }

    template <typename Rng>
    auto it_min(Rng range) {
      return ::numcpp::it_min(range.begin(),range.end());
    }
  }

  /*===STD Algorithms for Ranges===*/

  namespace ranges {

    template <typename Rng, typename UnaryPred>
    bool all_of(Rng range, UnaryPred p) {
      return std::all_of(range.begin(),range.end(),p);
    }

    template <typename Rng, typename UnaryPred>
    bool any_of(Rng range, UnaryPred p) {
      return std::any_of(range.begin(),range.end(),p);
    }

    template <typename Rng, typename UnaryPred>
    bool none_of(Rng range, UnaryPred p) {
      return std::none_of(range.begin(),range.end(),p);
    }

    template <typename Rng, typename UnaryFunc>
    UnaryFunc for_each(Rng range, UnaryFunc f) {
      return std::for_each(range.begin(),range.end(),f);
    }

    template <typename ExecutionPolicy, typename Rng, typename UnaryFunc>
    void for_each(ExecutionPolicy&& policy, Rng range, UnaryFunc f) {
      return std::for_each(policy, range.begin(),range.end(),f);
    }

    template <typename Rng, typename T>
    typename iterator_traits<typename Rng::iterator>::difference_type count(Rng range, T const& value) {
      return std::count(range.begin(),range.end(),value);
    }

    template <typename Rng, typename UnaryPred>
    typename iterator_traits<typename Rng::iterator>::difference_type count_if(Rng range, UnaryPred p) {
      return std::count_if(range.begin(),range.end(),p);
    }

    template <typename ExecutionPolicy, typename Rng, typename T>
    typename iterator_traits<typename Rng::iterator>::difference_type count(ExecutionPolicy&& policy, Rng range, T const& value) {
      return std::count(policy,range.begin(),range.end(),value);
    }

    template <typename ExecutionPolicy, typename Rng, typename UnaryPred>
    typename iterator_traits<typename Rng::iterator>::difference_type count_if(ExecutionPolicy&& policy, Rng range, UnaryPred p) {
      return std::count_if(policy,range.begin(),range.end(),p);
    }
  }
}

#endif
