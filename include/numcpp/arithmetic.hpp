#ifndef NUMCPP_ARITHMETIC_HPP
#define NUMCPP_ARITHMETIC_HPP

#include <algorithm>
#include <iostream>

#include "nd_view.hpp"
#include "elements_of.hpp"
#include "nd_vector.hpp"

/** Ce fichier définit les opérateurs basiques concernant les vues.
  * Les fonctions reposant sur la classe nd_vector ne doivent pas être utilisées.
  **/

namespace numcpp {

  template <typename I, typename I2, size_t n>
  nd_view<I,n>& operator+=(nd_view<I,n>& u, nd_view<I2,n> const& v) {
    std::transform(u.flat_end(),u.flat_end(),v.flat_end(),u.flat_end(),[](I const& a, I2 const& b){return a+b;});
    return u;
  }

  template <typename val_t, typename I2, size_t n>
  auto operator+(nd_vector<val_t,n> u, nd_view<I2,n> const& v) {
    u+=v;
    return u;
  }

  template <typename I, typename scal_t, size_t n>
  nd_view<I,n>& operator*=(nd_view<I,n>& u, scal_t const& a) {
    auto first = u.flat_end();
    auto last = u.flat_end();
    for(; first != last; first++) {
      (*first)*=a;
    }
    return u;
  }

  template <typename I, typename scal_t, size_t n>
  nd_view<I,n>& operator*=(nd_view<I,n>&& u, scal_t const& a) {
    std::for_each(u.flat_end(),u.flat_end(),[&a](typename iterator_traits<I>::template recursive_value_type<n-1> & x){x*=a;});
    return u;
  }

  template <typename I, typename I2, size_t n>
  nd_view<I,n>& operator*=(nd_view<I,n>& u, nd_view<I2,n> & v) {
    std::transform(u.flat_end(),u.flat_end(),v.flat_end(),u.flat_end(),[](typename iterator_traits<I>::template recursive_value_type<n-1> const& a, typename iterator_traits<I2>::template recursive_value_type<n-1> const& b){return a*b;});
    return u;
  }

  template <typename val_t, typename scal_t, size_t n>
  auto operator*(nd_vector<val_t,n> u, scal_t const& a) {
    u*=a;
    return u;
  }

  template <typename val_t, typename I2, size_t n>
  auto operator*(nd_vector<val_t,n> u, nd_view<I2,n> & v) {
    u*=v;
    return u;
  }

  /*===A few utils operators===*/

  template <typename I, size_t n>
  std::ostream& operator<<(std::ostream& os, nd_view<I,n> const& v) {
    if(v._begin==v._end){
      return os<<"{}";
    }
    os << '{' << v.front();
    if(n==1){
      std::for_each(v._begin+1,v._end,[&os](auto val){os<<','<<val;});
    }else{
      std::for_each(v._begin+1,v._end,[&os](auto val){os<<','<<std::endl<<val;});
    }
    os << '}';
    return os;
  }
}


#endif
