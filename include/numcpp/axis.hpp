#ifndef NUMCPP_AXIS_HPP
#define NUMCPP_AXIS_HPP

#include "utils.hpp"
#include "bounds.hpp"

/** Ce fichier contient les utilitaires nécessaires à la manipulation des axes
  * et à leur interversion
  * Ils sont notamment utilisés par la fonction axis_view
  */

namespace numcpp {

  template <size_t n, size_t a, size_t ...is>
  struct nth_axis_of_pack{
    static constexpr size_t value = nth_axis_of_pack<n-1ul,is...>::value; //BAAAAAAAAAD
  };

  template <size_t a, size_t ...is>
  struct nth_axis_of_pack<0ul,a,is...>{
    static constexpr size_t value = a;
  };

  template <size_t moved, size_t to_update>
  inline constexpr size_t update_axis_number = (moved!=to_update) * (to_update+(to_update<moved));

  template <typename view_t, size_t n>
  auto move_axis(view_t view) {
    return view.sequence_view([constant_part = utils::tuple_repeat<seq_bounds,n>::function(all)](size_t a){return std::tuple_cat(constant_part,std::tuple(a));});
  }

  template <typename view_t, size_t to_move, size_t ...is>
  struct adapt_view_axis_impl{
    static auto adapt_view_axis(view_t view){
      auto tmp=move_axis<view_t,nth_axis_of_pack<to_move,is...>::value>(view);
      return adapt_view_axis_impl<decltype(tmp),to_move-1ul,update_axis_number<nth_axis_of_pack<to_move,is...>::value,is>...>::adapt_view_axis(tmp);
    }
  };

  template <typename view_t, size_t...is>
  struct adapt_view_axis_impl<view_t,0ul,is...>{
    static auto adapt_view_axis(view_t view){
      return move_axis<view_t,nth_axis_of_pack<0ul,is...>::value>(view);
    }
  };

  template <typename range_t, size_t... is>
  auto make_axis_view(range_t range){
    return adapt_view_axis_impl<utils::view_of_t<range_t>,sizeof...(is)-1,is...>::adapt_view_axis(range);
  }

}

#endif
