#ifndef NUMCPP_BOUNDS_HPP
#define NUMCPP_BOUNDS_HPP

/** Ce fichier définit la structure seq_bounds et l'objet all;
  */

namespace numcpp {
  struct seq_bounds {
    std::optional<long> min;
    std::optional<long> max;
  } all;
}

#endif
