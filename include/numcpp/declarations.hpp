#ifndef NUMCPP_DECLARATIONS_HPP
#define NUMCPP_DECLARATIONS_HPP

/** Ce fichier contient les déclarations d'objets numcpp, nécessaires lors de
  * la compilation avant même d'être implémentées
  */

namespace numcpp {
  struct seq_bounds;

  template <typename I>
  struct iterator_traits;

  template <typename T>
  class range_traits;

  template <typename val_t, size_t n>
  class nd_vector;

  template <typename I, size_t n>
  class nd_view;

  template <typename I, size_t n>
  class viewified_iterator;

  template <typename I, size_t n>
  class flat_iterator_;

  template <typename view_t, typename ret_t, typename index_t=size_t>
  class sequence_iterator_;

  namespace experimental{
    template <typename val_t, size_t n>
    class nd_vector;
  }
}

#endif
