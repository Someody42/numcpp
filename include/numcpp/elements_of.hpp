#ifndef NUMCPP_ELEMENTS_OF_HPP
#define NUMCPP_ELEMENTS_OF_HPP

#include "nd_view.hpp"

/** Ce module est déprécié. Ne pas inclure
  */

namespace numcpp {

  template <typename view_t>
  class elements_of {
  private:
    view_t _view;
  public:
    elements_of(view_t view) : _view(view) {}
    operator view_t() {return _view;}
  };
}

#endif
