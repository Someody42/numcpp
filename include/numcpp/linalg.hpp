#ifndef NUMCPP_LINALG_HPP
#define NUMCPP_LINALG_HPP

#include <numeric>

#include "nd_vector.hpp"
#include "arithmetic.hpp"
#include "algorithm.hpp"

/** Ce fichier contient des algorithmes et utilitaires d'algèbres linéaires.
  * Il n'a pas été mis à jour lors du dernier changement majeur.
  * Ne pas utiliser avant son refactoring
  */

namespace numcpp {
  template <typename val_t, typename val_t2>
  auto dot(vector<val_t> const& u, vector<val_t2> const& v, bool check=true) {
    if(check && u.size()!=v.size()){
      throw("Uncompatible shapes");
    }
    return dot(u.begin(),u.end(),v.begin());
  }

  template <typename val_t, typename val_t2>
  auto matProd(matrix<val_t> const& A, matrix<val_t2> const& B,bool check=true) {
    typedef decltype(A[0][0]*B[0][0]) ret_val_t;
    if(check && (!A.isRectangular() || !B.isRectangular())) {
      throw("Not a matrix");
    }
    if(check && std::get<1>(A.rectangularShape()) != std::get<0>(B.rectangularShape())) {
      throw("Uncompatible shapes");
    }
    matrix<ret_val_t> ret(std::make_tuple(std::get<0>(A.rectangularShape()),std::get<1>(B.rectangularShape())));
    for (size_t i = 0; i < std::get<0>(A.rectangularShape()); i++) {
      for (size_t j = 0; j < std::get<1>(B.rectangularShape()); j++) {
        auto gen = [j](long n){return std::make_tuple(n,j);};
        ret[i][j] = dot(A[i].begin(),A[i].end(),B.sequence_begin(std::function(gen)));
      }
    }
    return ret;
  }

  template <typename val_t, typename val_t2>
  auto matProd(matrix<val_t> const& A, vector<val_t2> const& u, bool check=true) {
    typedef decltype(A[0][0]*u[0]) ret_val_t;
    auto U = matrix<val_t2>(std::make_tuple(u.size(),1));
    std::copy(u.begin(),u.end(),U.recursive_begin());
    auto M = matProd(A,U,check);
    return vector<ret_val_t>(M.recursive_begin(),M.recursive_end());
  }

  template <typename val_t>
  auto transpose(matrix<val_t> A, bool check=true) {
    if(check && !A.isRectangular()){
      throw("Not a matrix");
    }
    auto shape = A.rectangularShape();
    matrix<val_t> ret(std::make_tuple(std::get<1>(shape),std::get<0>(shape)));
    for (size_t i = 0; i < std::get<0>(shape); i++) {
      for (size_t j = 0; j < std::get<1>(shape); j++) {
        ret[j][i] = A[i][j];
      }
    }
    return ret;
  }
}

#endif
