#ifndef NUMCPP_MACROS_HPP
#define NUMCPP_MACROS_HPP

#define EXPERIMENTAL_FEATURES

/* Cette macro simplifie l'utilisation de std::enable_if pour un paramètre inutile,
 * afin de déclencher la SFINAE*/
#define ENABLE_IF(condition) std::enable_if_t<(condition),::numcpp::utils::dummy_type> = ::numcpp::utils::dummy_val

#endif
