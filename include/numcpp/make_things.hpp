#ifndef NUMCPP_MAKE_THINGS_HPP
#define NUMCPP_MAKE_THINGS_HPP

#include <type_traits>

#include "nd_view.hpp"

/** Ce fichier contient les raccourcis syntaxiques pour la création des vues
  * en déduisant le type d'itérateurs de la vue
  */

namespace numcpp{

  template <size_t n, typename I>
  auto make_nd_view(I a, I b){return nd_view<I,n>(a,b);}

  template <size_t n, typename range_t>
  auto make_nd_view(range_t& rng){return make_nd_view<n>(rng.begin(),rng.end());}

}

#endif
