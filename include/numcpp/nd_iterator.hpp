#ifndef NUMCPP_ND_ITERATOR_HPP
#define NUMCPP_ND_ITERATOR_HPP

#include "utils.hpp"
#include "declarations.hpp"

#include <functional>
#include <optional>

/** Ce fichier contient les différents itérateurs utilisés par la librairie
  */

namespace numcpp {

  template <typename I, size_t n>
  class viewified_iterator {
  private:
    I _underlying;
  public:
    using difference_type = typename iterator_traits<I>::difference_type;
    using value_type = typename iterator_traits<I>::value_type;
    using pointer = typename iterator_traits<I>::pointer;
    using reference = utils::viewified_iterator_reference_t<I,n>;
    using iterator_category = typename std::random_access_iterator_tag;

    viewified_iterator(I it) : _underlying(it) {}

    viewified_iterator() : _underlying() {}

    reference operator*() const {
      return *_underlying;
    }

    utils::arrow_solver<reference> operator->() const {
      return utils::arrow_solver(operator*());
    }

    viewified_iterator& operator++() {
      std::advance(_underlying,1);
      return *this;
    }

    viewified_iterator& operator--() {
      std::advance(_underlying,-1);
      return *this;
    }

    viewified_iterator operator++(int dummy) {
      viewified_iterator ret = *this; operator++(); return ret;
    }

    viewified_iterator operator--(int dummy) {
      viewified_iterator ret = *this; operator--(); return ret;
    }

    viewified_iterator& operator+=(difference_type a) {
      std::advance(_underlying,a);
      return *this;
    }

    viewified_iterator& operator-=(difference_type a) {
      return operator+=(-a);
    }

    friend viewified_iterator operator+(viewified_iterator it, difference_type a) {
      return it+=a;
    }

    friend viewified_iterator operator-(viewified_iterator it, difference_type a) {
      return it-=a;
    }

    friend viewified_iterator operator+(difference_type a, viewified_iterator it) {
      return it+a;
    }

    friend viewified_iterator operator-(difference_type a, viewified_iterator it) {
      return it-a;
    }

    bool operator==(viewified_iterator const& other) const {
      return _underlying==other._underlying;
    }

    bool operator!=(viewified_iterator const& other) const {
      return !operator==(other);
    }

    difference_type operator-(viewified_iterator const& other) const {
      return std::distance(other._underlying,_underlying);
    }

    difference_type operator>(viewified_iterator const& other) const {
      return operator-(other) > 0;
    }

    difference_type operator<(viewified_iterator const& other) const {
      return operator-(other) < 0;
    }

    difference_type operator>=(viewified_iterator const& other) const {
      return !(operator<(other));
    }

    difference_type operator<=(viewified_iterator const& other) const {
      return !(operator>(other));
    }

    reference operator[](difference_type a) const {
      return *(*this+a);
    }
  };

  template <typename I, size_t n>
  class flat_iterator_ {
  public:
    nd_view<I,n> _view;
    typename utils::iterator_on_t<nd_view<I,n>> _top_level_it;
    utils::flat_iterator_type_t<utils::recursive_iterator_type_t<I,1ul>,n-1ul> _bottom_level_it;

    using difference_type = long;
    using value_type = typename range_traits<nd_view<I,n>>::value_type;
    static constexpr bool is_mutable = iterator_traits<I>::is_mutable;
    using pointer = utils::adapt_constness<value_type*,!is_mutable>;
    using reference = utils::adapt_constness<value_type&,!is_mutable>;
    using iterator_category = std::random_access_iterator_tag;

    flat_iterator_& operator+=(difference_type a) {
      if(isGeneric()) {
        return *this;
      }
      difference_type overflow;
      _bottom_level_it += a;
      /*Gestion du dépassement*/
      if(_bottom_level_it<(*_top_level_it).flat_begin()){
        overflow = _bottom_level_it-(*_top_level_it).flat_begin();
        if(_top_level_it>_view.begin()){ //Si on peut, on relègue l'overflow au voisin
        _top_level_it--;
        _bottom_level_it = (*_top_level_it).flat_end();
        return operator+=(overflow);
      }
      return *this; //Sinon, on garde l'overflow pour nous
    }else if(_bottom_level_it>=(*_top_level_it).flat_end()){
      overflow = _bottom_level_it-(*_top_level_it).flat_end();
      if(_top_level_it<_view.end()-1){ //Si on peut, on relègue l'overflow au voisin
      _top_level_it++;
      _bottom_level_it = (*_top_level_it).flat_begin();
      return operator+=(overflow);
    }
    return *this; //Sinon, on garde l'overflow pour nous
  }else{
    return *this;
  }
}

flat_iterator_& operator-=(difference_type a) {
  return operator+=(-a);
}

flat_iterator_& operator++() {
  return operator+=(1);
}

flat_iterator_& operator--() {
  return operator-=(1);
}

flat_iterator_ operator+(difference_type a) const {
  flat_iterator_ ret(*this);
  return ret+=a;
}

flat_iterator_ operator-(difference_type a) const {
  flat_iterator_ ret(*this);
  return ret-=a;
}

flat_iterator_ operator++(int x) {
  flat_iterator_ ip=*this; operator++(); return ip;
}

flat_iterator_ operator--(int x) {
  flat_iterator_ ip=*this; operator--(); return ip;
}

friend difference_type operator-(flat_iterator_ a, flat_iterator_ b){
  difference_type difference = 0;
  if(a.isGeneric()) {
    if(b.isGeneric()) {
      return 0;
    }else{
      return std::numeric_limits<difference_type>::min();
    }
  }else if(b.isGeneric()) {
    return std::numeric_limits<difference_type>::max();
  }

  for(;;) {
    if(a._top_level_it==b._top_level_it){
      return difference+(a._bottom_level_it-b._bottom_level_it);
    }else if(a._top_level_it<b._top_level_it){
      difference += a._bottom_level_it - (*a._top_level_it).flat_end();
      a._top_level_it++;
      a._bottom_level_it = (*a._top_level_it).flat_begin();
    }else{
      difference -= b._bottom_level_it - (*b._top_level_it).flat_end();
      b._top_level_it++;
      b._bottom_level_it = (*b._top_level_it).flat_begin();
    }
  }
}

reference operator*() const {
  return *_bottom_level_it;
}

reference operator[](difference_type a) const {
  return *(*this+a);
}

bool operator==(flat_iterator_ const& other) const {
  return (isGeneric() && other.isGeneric()) || (_top_level_it == other._top_level_it && _bottom_level_it== other._bottom_level_it);
}

bool operator!=(flat_iterator_ const& other) const {
  return !operator==(other);
}

bool operator<(flat_iterator_ const& other) const {
  return _top_level_it<other._top_level_it || _bottom_level_it<other._bottom_level_it;
}

bool operator>(flat_iterator_ const& other) const {
  return _top_level_it>other._top_level_it || _bottom_level_it>other._bottom_level_it;
}

bool operator>=(flat_iterator_ const& other) const {
  return !operator<(other);
}

bool operator<=(flat_iterator_ const& other) const {
  return !operator>(other);
}

bool isGeneric() const {
  return _view._begin==typename utils::iterator_on_t<nd_view<I,n>>() && _view._end==utils::iterator_on_t<nd_view<I,n>>();
}

flat_iterator_(nd_view<I,n> const& view, decltype(_top_level_it) top_level_it, decltype(_bottom_level_it) bottom_level_it = decltype(_bottom_level_it)()) : _view(view), _top_level_it(top_level_it), _bottom_level_it(bottom_level_it) {}

flat_iterator_() : _view() {}

virtual ~flat_iterator_() = default;
};

  template <typename view_t, typename ret_t, typename index_t>
  class sequence_iterator_ {
  public:
    using I = utils::iterator_on_t<view_t>;
    static constexpr size_t n = utils::dimensionality_v<view_t>;
    using difference_type = std::make_signed_t<index_t>;
  protected:
    std::optional<nd_view<I,n>> _view;
    std::function<ret_t(index_t)> _generator;
    difference_type _pos;
  public:
    using reference = utils::subscript_result_type_t<nd_view<I,n>,ret_t>;
    using value_type = std::remove_reference_t<reference>;
    static constexpr bool is_mutable = iterator_traits<I>::is_mutable;
    using pointer = utils::adapt_constness<value_type*,!is_mutable>;
    using iterator_category = std::random_access_iterator_tag;

    reference operator*() const{
      return (*_view)[_generator((index_t)_pos)];
    }

    utils::arrow_solver<reference> operator->() const{
      return utils::arrow_solver(operator*());
    }

    sequence_iterator_& operator+=(difference_type a) {
      _pos+=a;
      return *this;
    }

    sequence_iterator_& operator-=(difference_type a) {
      return operator+=(-a);
    }

    sequence_iterator_& operator++() {
      return operator+=(1);
    }

    sequence_iterator_& operator--() {
      return operator+=(-1);
    }

    sequence_iterator_ operator++(int dummy) {
      sequence_iterator_ ret = *this; operator++(); return ret;
    }

    sequence_iterator_ operator--(int dummy) {
      sequence_iterator_ ret = *this; operator--(); return ret;
    }

    friend sequence_iterator_ operator+(sequence_iterator_ it, difference_type a) {
      return it+=a;
    }

    friend sequence_iterator_ operator-(sequence_iterator_ it, difference_type a) {
      return it-=a;
    }

    friend sequence_iterator_ operator+(difference_type a, sequence_iterator_ it) {
      return it+a;
    }

    friend sequence_iterator_ operator-(difference_type a, sequence_iterator_ it) {
      return it-a;
    }

    bool is_generic() const {
      return !((bool)_view);
    }

    bool is_out_of_range() const {
      if(is_generic()){
        return true;
      }
      try {
        _view->at(_generator(_pos));
      } catch(...) {
        return true;
      }
      return false;
    }

    template <typename ret_t2, typename index_t2>
    bool operator==(sequence_iterator_<view_t,ret_t2,index_t2> const& other) const {
      if(is_generic()){
        return other.is_out_of_range();
      }
      if(other.is_generic()){
        return is_out_of_range();
      }
      return _pos == other._pos; //TODO : améliorer
    }

    template <typename ret_t2, typename index_t2>
    bool operator!=(sequence_iterator_<view_t,ret_t2,index_t2> const& other) const {
      return !(operator==(other));
    }

    template <typename ret_t2, typename index_t2>
    difference_type operator-(sequence_iterator_<view_t,ret_t2,index_t2> const& other) const {
      if(is_generic()){
        return other.is_generic() ? 0 : std::numeric_limits<difference_type>::min();
      }else if(other.is_generic()) {
        return std::numeric_limits<difference_type>::max();
      }
      return _pos-other._pos;
    }

    template <typename ret_t2, typename index_t2>
    difference_type operator>(sequence_iterator_<view_t,ret_t2,index_t2> const& other) const {
      return operator-(other) > 0;
    }

    template <typename ret_t2, typename index_t2>
    difference_type operator<(sequence_iterator_<view_t,ret_t2,index_t2> const& other) const {
      return operator-(other) < 0;
    }

    template <typename ret_t2, typename index_t2>
    difference_type operator>=(sequence_iterator_<view_t,ret_t2,index_t2> const& other) const {
      return !(operator<(other));
    }

    template <typename ret_t2, typename index_t2>
    difference_type operator<=(sequence_iterator_<view_t,ret_t2,index_t2> const& other) const {
      return !(operator>(other));
    }

    auto generator() const{
      return _generator;
    }

    auto view() const{
      return *_view;
    }

    auto pos() const{
      return _pos;
    }

    reference operator[](difference_type a) const{
      return *(*this+a);
    }

    sequence_iterator_() {}

    sequence_iterator_(sequence_iterator_ const& other) : _view(other._view), _generator(other._generator), _pos(other._pos) {}

    sequence_iterator_(decltype(_view) const& view, std::function<ret_t(index_t)> generator, difference_type initial_pos) : _view(view), _generator(generator), _pos(initial_pos) {}
  };
}

#endif
