#ifndef NUMCPP_ND_VECTOR_HPP
#define NUMCPP_ND_VECTOR_HPP

#include "nd_view.hpp"
#include "macros.hpp"
#include "make_things.hpp"

#include <vector>

/** Ce fichier définit un conteneur multi-dimensionnel basé sur std::vector
  * et fournissant une interface similaire à celle d'une vue.
  * Sa mise à jour est en cours. Ne pas utiliser.
  */

namespace numcpp {


  #ifdef EXPERIMENTAL_FEATURES
  namespace experimental{
    template <typename val_t,size_t n>
    class nd_vector : public std::vector<::numcpp::utils::decrease_vector_dim_t<val_t,n>>{

    public:
      using std_type = std::vector<::numcpp::utils::decrease_vector_dim_t<val_t,n>>;
      using decreased_type = ::numcpp::utils::decrease_vector_dim_t<val_t,n>;
      using std_type::size;
      using std_type::begin;
      using std_type::end;

      explicit nd_vector() : std_type() { }

      template <size_t d = n>
      nd_vector( size_t count,const decreased_type& value, ENABLE_IF(d>1ul)) : std_type( count, value ) { }

      template <size_t d = n>
      nd_vector( size_t count,const val_t & value, ENABLE_IF(d==1ul)) : std_type( count, value ) {}

      explicit nd_vector( size_t count ) : std_type(count) { }

      template< class InputIt >
      nd_vector( InputIt first, InputIt last) : std_type(first,last) { }

      nd_vector( nd_vector<val_t,n> const& other ) : std_type(other.begin(),other.end()) { }
      nd_vector( nd_vector<val_t,n> const&& other ) : std_type(other.begin(),other.end()) { }
      nd_vector( const std_type& other ) : std_type(other) { }
      nd_vector( const std_type&& other ) : std_type(other) { }

      template <size_t d = n>
      nd_vector( std::initializer_list<nd_vector<val_t,n-1>> init, ENABLE_IF(d>1ul)) : std_type(init) { }

      template <size_t d = n>
      nd_vector( std::initializer_list<val_t> init, ENABLE_IF(d==1ul)) : std_type(init) { }

      template <typename def_val_t, typename T, typename ...Ts, size_t d=n>
      nd_vector( std::tuple<T,Ts...> shape, def_val_t defaultVal, ENABLE_IF(d>1ul) ) : nd_vector(std::get<0>(shape),decreased_type(hana::drop_front(shape),defaultVal)) { }

      template <typename def_val_t, typename T>
      nd_vector( std::tuple<T> shape, def_val_t defaultVal ) : nd_vector(std::get<0>(shape),defaultVal) { }

      template <typename T, typename ...Ts, size_t d=n>
      nd_vector( std::tuple<T,Ts...> shape, ENABLE_IF(d>1ul) ) : nd_vector(std::get<0>(shape),nd_vector<val_t,n-1>(hana::drop_front(shape))) { }

      template <typename T>
      nd_vector( std::tuple<T> shape ) : nd_vector(std::get<0>(shape)) { }

      template <typename T>
      nd_vector( nd_view<T,n> const& view ) : std_type(view.begin(),view.end()) { }

      template <typename T>
      nd_vector( nd_view<T,n> const&& view ) : std_type(view.begin(),view.end()) { }


      auto view() {
        return make_nd_view(*this);
      }

      operator nd_view<utils::iterator_on_t<std_type>,n> () {
        return view();
      }

      auto flat_begin() {
        return this->view().flat_begin();
      }

      auto flat_end() {
        return this->view().flat_end();
      }

      template <typename gen_t>
      auto sequence_begin(gen_t gen){
        return this->view().sequence_begin(gen);
      }

      template <typename gen_t>
      auto sequence_end(gen_t gen){
        return this->view().sequence_end(gen);
      }

      auto front() {
        return operator[](0);
      }

      auto back() {
        return operator[](size()-1);
      }

      utils::subscript_result_type_t<nd_vector,size_t> operator[](size_t i) {
        return std_type::operator[](i);
      }

      template <typename T, typename U, typename ...Ts>
      utils::subscript_result_type_t<nd_vector,std::tuple<T,U,Ts...>> operator[](std::tuple<T,U,Ts...> index) const{
        static_assert(std::tuple_size_v<std::tuple<T,U,Ts...>> <= n, "You are going too deep. Remember not to group together two dimensionality layers. ");
        T first = std::get<0>(index);
        return (*this)[first][hana::drop_front(index)];
      }

      template <typename T>
      utils::subscript_result_type_t<nd_vector,std::tuple<T>> operator[](std::tuple<T> index) const {
        T first = std::get<0>(index);
        return (*this)[first];
      }

      template <typename T, typename ...Ts>
      utils::subscript_result_type_t<nd_vector,std::tuple<seq_bounds,T,Ts...>> operator[](std::tuple<seq_bounds,T,Ts...> index) const{
        auto bounds = std::get<0>(index);
        bounds.min = bounds.min.has_value() ? *bounds.min : 0l;
        bounds.max = bounds.max.has_value() ? *bounds.max : size();
        return sequence_view([idx=index](size_t a){return std::tuple_cat(std::tuple(a),hana::drop_front(idx));},bounds);
      }

      utils::subscript_result_type_t<nd_vector,seq_bounds> operator[](seq_bounds bounds) const{
        bounds.min = bounds.min.has_value() ? *bounds.min : 0l;
        bounds.max = bounds.max.has_value() ? *bounds.max : size();
        return sequence_view([](size_t a){return std::tuple(a);},bounds);
      }

      /*template <typename ...Ts>
      auto at(Ts... index) {
        return this->as_view().at(index...);
      }

      template <typename ...Ts>
      auto operator()(Ts... index) {
        return this->as_view()(index...);
      }*/

      auto flat_view(){
        return this->view().flat_view();
      }

      template <typename gen_t>
      auto sequence_view(gen_t gen) {
        return this->view().sequence_view(gen);
      }

      template <size_t... axis>
      auto axis_view() {
        return this->view().template axis_view<axis...>();
      }

    };

  }
  #endif

  template <typename val_t, size_t n>
  class nd_vector : public nd_view<typename std::vector<nd_vector<val_t,n-1>>::iterator,n>{
  private:

  public:
    std::vector<nd_vector<val_t,n-1>> _vector;
    using nd_view<typename std::vector<nd_vector<val_t,n-1>>::iterator,n>::_begin;
    using nd_view<typename std::vector<nd_vector<val_t,n-1>>::iterator,n>::_end;

    using value_type = val_t;

    void updateIterators() {
      _begin = _vector.begin();
      _end = _vector.end();
      for( auto & v : _vector) {
        v.updateIterators();
      }
    }

    explicit nd_vector( const std::allocator<nd_vector<value_type,n-1>>& alloc = std::allocator<nd_vector<value_type,n-1>>() ) : _vector( alloc ) {updateIterators(); }
    nd_vector( size_t count,const nd_vector<value_type,n-1>& value,const std::allocator<nd_vector<value_type,n-1>>& alloc = std::allocator<nd_vector<value_type,n-1>>()) : _vector( count, value, alloc ) {updateIterators(); }
    explicit nd_vector( size_t count ) : _vector(count) {updateIterators(); }

    template< class InputIt >
    nd_vector( InputIt first, InputIt last,const std::allocator<nd_vector<value_type,n-1>>& alloc = std::allocator<nd_vector<value_type,n-1>>() ) : _vector(first,last,alloc) {updateIterators(); }

    nd_vector( nd_vector<val_t,n> const& other ) : _vector(other.begin(),other.end()) {updateIterators(); }
    nd_vector( nd_vector<val_t,n> const&& other ) : _vector(other.begin(),other.end()) {updateIterators(); }
    nd_vector( const std::vector<nd_vector<value_type,n-1>>& other ) : _vector(other) {updateIterators(); }
    nd_vector( const std::vector<nd_vector<value_type,n-1>>& other, const std::allocator<value_type>& alloc ) : _vector(other,alloc) {updateIterators(); }
    nd_vector( std::vector<nd_vector<value_type,n-1>>&& other ) : _vector(other) {updateIterators(); }
    nd_vector( std::vector<nd_vector<value_type,n-1>>&& other, const std::allocator<nd_vector<value_type,n-1>>& alloc ) : _vector(other, alloc) {updateIterators(); }
    nd_vector( std::initializer_list<nd_vector<value_type,n-1>> init, const std::allocator<nd_vector<value_type,n-1>>& alloc = std::allocator<nd_vector<value_type,n-1>>() ) : _vector(init,alloc) {updateIterators(); }

    template <typename def_val_t, typename T, typename ...Ts>
    nd_vector( std::tuple<T,Ts...> shape, def_val_t defaultVal ) : nd_vector(std::get<0>(shape),nd_vector<value_type,n-1>(hana::drop_front(shape),defaultVal)) {updateIterators(); }

    template <typename def_val_t, typename T>
    nd_vector( std::tuple<T> shape, def_val_t defaultVal ) : nd_vector(std::get<0>(shape),defaultVal) {updateIterators(); }

    template <typename T, typename ...Ts>
    nd_vector( std::tuple<T,Ts...> shape ) : nd_vector(std::get<0>(shape),nd_vector<value_type,n-1>(hana::drop_front(shape))) {updateIterators(); }

    template <typename T>
    nd_vector( std::tuple<T> shape ) : nd_vector(std::get<0>(shape)) {updateIterators(); }

    template <typename T>
    nd_vector( nd_view<T,n> const& view ) : _vector(view.begin(),view.end()) {updateIterators(); }

    template <typename T>
    nd_vector( nd_view<T,n> const&& view ) : _vector(view.begin(),view.end()) {updateIterators(); }

    template <typename I>
    void copyShapeOf( nd_view<I,n> const& other, val_t defaultVal ) {
      _vector = std::vector<nd_vector<val_t,n-1>>(other.size());
      for (size_t i = 0; i < other.size(); i++) {
        _vector[i].copyShapeOf(other[i],defaultVal);
      }
      updateIterators();
    }

    operator nd_view<typename std::vector<nd_vector<val_t,n-1>>::iterator,n>() {
      return nd_view<typename std::vector<nd_vector<val_t,n-1>>::iterator,n>(*this);
    }

    virtual ~nd_vector() = default;
  };

  template <typename val_t>
  class nd_vector<val_t,1> : public nd_view<typename std::vector<val_t>::iterator,1> {
  private:

  public:
    std::vector<val_t> _vector;
    using nd_view<typename std::vector<val_t>::iterator,1>::_begin;
    using nd_view<typename std::vector<val_t>::iterator,1>::_end;

    static constexpr size_t n = 1;

    using value_type = val_t;

    void updateIterators() {
      _begin = _vector.begin();
      _end = _vector.end();
    }

    explicit nd_vector( const std::allocator<value_type>& alloc = std::allocator<value_type>() ) :   _vector( alloc ) {updateIterators(); }
    nd_vector( size_t count,const value_type& value,const std::allocator<value_type>& alloc = std::allocator<value_type>()) :   _vector( count, value, alloc ) {updateIterators(); }
    explicit nd_vector( size_t count ) :   _vector(count) {updateIterators(); }

    template< class InputIt >
    nd_vector( InputIt first, InputIt last,const std::allocator<value_type>& alloc = std::allocator<value_type>() ) :   _vector(first,last,alloc) {updateIterators(); }

    nd_vector( nd_vector<val_t,1> const& other ) : _vector(other.begin(),other.end()) {updateIterators(); }
    nd_vector( nd_vector<val_t,1> const&& other ) : _vector(other.begin(),other.end()) {updateIterators(); }
    nd_vector( const std::vector<value_type>& other ) :   _vector(other) {updateIterators(); }
    nd_vector( const std::vector<value_type>& other, const std::allocator<value_type>& alloc ) :   _vector(other,alloc) {updateIterators(); }
    nd_vector( std::vector<value_type>&& other ) :   _vector(other) {updateIterators(); }
    nd_vector( std::vector<value_type>&& other, const std::allocator<value_type>& alloc ) :   _vector(other, alloc) {updateIterators(); }
    nd_vector( std::initializer_list<value_type> init, const std::allocator<value_type>& alloc = std::allocator<value_type>() ) :  _vector(init,alloc) {updateIterators(); }

    template <typename def_val_t, typename T>
    nd_vector( std::tuple<T> shape, def_val_t defaultVal ) : nd_vector(std::get<0>(shape),defaultVal) {updateIterators(); }

    template <typename T>
    nd_vector( std::tuple<T> shape ) : nd_vector(std::get<0>(shape)) {updateIterators(); }

    template <typename T>
    nd_vector( nd_view<T,1> const& view ) : _vector(view.begin(),view.end()) {updateIterators(); }

    template <typename T>
    nd_vector( nd_view<T,1> const&& view ) : _vector(view.begin(),view.end()) {updateIterators(); }

    template <typename I>
    void copyShapeOf( nd_view<I,1> const& other, val_t defaultVal ) {
      _vector = std::vector<value_type>(other.size(),defaultVal);
    }

    operator nd_view<typename std::vector<val_t>::iterator,1>() {
      return nd_view<typename std::vector<val_t>::iterator,1>(*this);
    }

  };

  template <typename val_t>
  using vector = nd_vector<val_t,1>;

  template <typename val_t>
  using matrix = nd_vector<val_t,2>;
}

#endif
