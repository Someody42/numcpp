#ifndef NUMCPP_ND_VIEW_HPP
#define NUMCPP_ND_VIEW_HPP

#include "declarations.hpp"
#include "elements_of.hpp"
#include "utils.hpp"
#include "nd_iterator.hpp"
#include "bounds.hpp"
#include "axis.hpp"

#include <functional>

#include <boost/hana.hpp>
#include <boost/hana/ext/std/tuple.hpp>

namespace hana = boost::hana;

/** Ce fichier définit la classe nd_view
  */

namespace numcpp {
  template <typename I, size_t n>
  class nd_view{
  public:
    static constexpr bool is_mutable = iterator_traits<I>::is_mutable;
    static constexpr size_t dimensionality = n;
    using iterator = utils::iterator_on_t<nd_view>;
    using flat_iterator = utils::flat_iterator_type_t<I,n>;
    template <typename ret_t, typename index_t = size_t>
    using sequence_iterator = sequence_iterator_<nd_view,ret_t,index_t>;
    using value_type = typename range_traits<nd_view>::value_type;

    nd_view() : _begin(), _end() {}

    template <typename iter_t>
    nd_view (iter_t begin, iter_t end) : _begin(begin), _end(end) {}

    template <typename range_t, typename = std::enable_if<is_mutable>>
    nd_view (range_t&& range) : nd_view(range.begin(), range.end()) {}

    template <typename range_t, typename = std::enable_if<!is_mutable>>
    nd_view (range_t const && range) : nd_view(range.begin(), range.end()) {}

    template <typename range_t, typename = std::enable_if<is_mutable>>
    nd_view (range_t& range) : nd_view(range.begin(), range.end()) {}

    template <typename range_t, typename = std::enable_if<!is_mutable>>
    nd_view (range_t const& range) : nd_view(range.begin(), range.end()) {}

    template <typename range_t, typename = std::enable_if<is_mutable>>
    nd_view& operator>>= (range_t&& range) {_begin=range.begin();_end=range.end();return *this;}

    template <typename range_t, typename = std::enable_if<!is_mutable>>
    nd_view& operator>>= (range_t const&& range) {_begin=range.begin();_end=range.end();return *this;}

    template <typename range_t, typename = std::enable_if<is_mutable>>
    nd_view& operator>>= (range_t& range) {_begin=range.begin();_end=range.end();return *this;}

    template <typename range_t, typename = std::enable_if<!is_mutable>>
    nd_view& operator>>= (range_t const& range) {_begin=range.begin();_end=range.end();return *this;}

    iterator begin() const {return _begin;}

    iterator end() const {return _end;}

    auto front() const {return *_begin;}

    auto back() const {return *(_end-1);}

    typename iterator_traits<iterator>::difference_type size() const {return _end-_begin;}

    bool empty() const {return _begin==_end;}

    utils::subscript_result_type_t<nd_view,size_t> operator[](size_t i) const{
      return _begin[i];
    }

    template <typename T, typename U, typename ...Ts>
    utils::subscript_result_type_t<nd_view,std::tuple<T,U,Ts...>> operator[](std::tuple<T,U,Ts...> index) const{
      static_assert(std::tuple_size_v<std::tuple<T,U,Ts...>> <= n, "You are going too deep. Remember not to group together two dimensionality layers. ");
      T first = std::get<0>(index);
      return (*this)[first][hana::drop_front(index)];
    }

    template <typename T>
    utils::subscript_result_type_t<nd_view,std::tuple<T>> operator[](std::tuple<T> index) const {
      T first = std::get<0>(index);
      return (*this)[first];
    }

    //*/
    template <typename T, typename ...Ts>
    utils::subscript_result_type_t<nd_view,std::tuple<seq_bounds,T,Ts...>> operator[](std::tuple<seq_bounds,T,Ts...> index) const{
      auto bounds = std::get<0>(index);
      bounds.min = bounds.min.has_value() ? *bounds.min : 0l;
      bounds.max = bounds.max.has_value() ? *bounds.max : size();
      return sequence_view([idx=index](size_t a){return std::tuple_cat(std::tuple(a),hana::drop_front(idx));},bounds);
    }

    utils::subscript_result_type_t<nd_view,seq_bounds> operator[](seq_bounds bounds) const{
      bounds.min = bounds.min.has_value() ? *bounds.min : 0l;
      bounds.max = bounds.max.has_value() ? *bounds.max : size();
      return sequence_view([](size_t a){return std::tuple(a);},bounds);
    }//*/

    utils::subscript_result_type_t<nd_view,size_t> at(size_t i) const{
      if(_begin+i>=_end){
        throw(std::out_of_range("a"));
      }
      return _begin[i];
    }

    template <typename T, typename U, typename ...Ts>
    utils::subscript_result_type_t<nd_view,std::tuple<T,U,Ts...>> at(std::tuple<T,U,Ts...> index) const{
      static_assert(std::tuple_size_v<std::tuple<T,U,Ts...>> <= n, "You are going too deep. Remember not to group together two dimensionality layers. ");
      T first = std::get<0>(index);
      return this->at(first).at(hana::drop_front(index));
    }

    template <typename T>
    utils::subscript_result_type_t<nd_view,std::tuple<T>> at(std::tuple<T> index) const {
      T first = std::get<0>(index);
      return at(first);
    }

    //*/
    template <typename T, typename ...Ts>
    utils::subscript_result_type_t<nd_view,std::tuple<seq_bounds,T,Ts...>> at(std::tuple<seq_bounds,T,Ts...> index) const{
      auto bounds = std::get<0>(index);
      if(bounds.min.has_value() && *bounds.min<0l){
        throw(std::out_of_range("b"));
      }
      if(bounds.max.has_value() && *bounds.max>size()){
        throw(std::out_of_range("c"));
      }
      bounds.min = bounds.min.has_value() ? *bounds.min : 0l;
      bounds.max = bounds.max.has_value() ? *bounds.max : size();
      std::for_each(begin(),end(),[idx=index](auto const& x){x.at(hana::drop_front(idx));});
      return sequence_view([idx=index](size_t a){return std::tuple_cat(std::tuple(a),hana::drop_front(idx));},bounds);
    }

    utils::subscript_result_type_t<nd_view,seq_bounds> at(seq_bounds bounds) const{
      if(bounds.min.has_value() && *bounds.min<0l){
        throw(std::out_of_range("b"));
      }
      if(bounds.max.has_value() && *bounds.max>size()){
        throw(std::out_of_range("c"));
      }
      bounds.min = bounds.min.has_value() ? *bounds.min : 0l;
      bounds.max = bounds.max.has_value() ? *bounds.max : size();
      return sequence_view([](size_t a){return std::tuple(a);},bounds);
    }//*/

    template <typename T, typename ...Ts>
    utils::subscript_result_type_t<nd_view,T,Ts...> operator()(T a, Ts ...bs) {
      return operator[](std::make_tuple(a,bs...));
    }

    template <size_t d = n>
    std::enable_if_t<(d>1ul),flat_iterator> flat_begin() {
      if(empty()){
        return flat_iterator(*this,_begin);
      }
      return flat_iterator(*this,_begin,front().flat_begin());
    }

    template <size_t d=n>
    std::enable_if_t<(d==1ul),flat_iterator> flat_begin() {
      return _begin;
    }

    template <size_t d=n>
    std::enable_if_t<(d>1ul),flat_iterator> flat_end() {
      if(empty()){
        return flat_begin();
      }
      return flat_iterator(*this,_end-1,back().flat_end());
    }

    template <size_t d=n>
    std::enable_if_t<(d==1ul),flat_iterator> flat_end() {
      return _end;
    }

    auto flat_view() {
      return nd_view<flat_iterator,1>(flat_begin(),flat_end());
    }

    template <typename ret_t, typename index_t>
    sequence_iterator<ret_t,index_t> sequence_begin(std::function<ret_t(index_t)> generator) const {
      return sequence_iterator<ret_t,index_t>(*this,generator,0);
    }

    template <typename ret_t, typename index_t>
    sequence_iterator<ret_t,index_t> sequence_end(std::function<ret_t(index_t)> generator) const {
      auto it = sequence_begin(generator);
      while(!it.is_out_of_range()){
        it++;
      }
      return it;
    }

    template <typename gen_t>
    auto sequence_begin(gen_t generator) const {
      return sequence_begin(std::function(generator));
    }

    template <typename gen_t>
    auto sequence_end(gen_t generator) const {
      return sequence_end(std::function(generator));
    }

    template <typename ret_t, typename index_t=size_t>
    auto _sequence_view(std::function<ret_t(index_t)> generator, seq_bounds bounds) const {
      auto begin = sequence_begin(generator);
      return nd_view<sequence_iterator<ret_t,index_t>,utils::resulting_dim_v<n,ret_t>+1>(bounds.min.has_value() ? begin+*bounds.min : begin, bounds.max.has_value() ? begin+*bounds.max : sequence_end(generator));
    }

    template <typename gen_t>
    auto sequence_view(gen_t generator, seq_bounds bounds = seq_bounds{}) const {
      return _sequence_view(std::function(generator), bounds);
    }

    template <size_t ...axis>
    auto axis_view() const{
      return make_axis_view<nd_view, axis...>(*this);
    }

    iterator _begin;
    iterator _end;
  };
}

#include "traits.hpp"

#endif
