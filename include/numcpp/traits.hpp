#ifndef NUMCPP_TRAITS_HPP
#define NUMCPP_TRAITS_HPP

#include <type_traits>
#include <functional>

#include "nd_view.hpp"

/** Ce fichier définit les utilitaires "traits" des types de la librairie.
  * Ils sont notamment utiles pour éviter les problèmes de "nested types".
  */

namespace numcpp {
  template <typename I>
  struct iterator_traits : public std::iterator_traits<I>{
    using typename std::iterator_traits<I>::value_type;
    using typename std::iterator_traits<I>::reference;
    using typename std::iterator_traits<I>::pointer;
    using typename std::iterator_traits<I>::difference_type;
    using typename std::iterator_traits<I>::iterator_category;
    static constexpr bool is_mutable = !std::is_const_v<decltype(*std::declval<I>())>;
  };

  template <typename range_t>
  class range_traits {
  public:
    using iterator_type = utils::iterator_on_t<range_t>;
    static constexpr size_t dimensionality = 1;
    using value_type = utils::recursive_value_type_t<range_t,dimensionality>;
    static constexpr bool is_const_view = std::is_const_v<range_t>;
  };

  template <typename val_t, size_t n>
  class range_traits<nd_vector<val_t,n>> {
  public:
    using iterator_type = utils::iterator_on_t<nd_vector<val_t,n>>;
    static constexpr size_t dimensionality = n;
    using value_type = val_t;
    static constexpr bool is_const_view = false;
  };

  template <typename I, size_t n>
  class range_traits<nd_view<I,n>> {
  public:
    using iterator_type = utils::iterator_on_t<nd_view<I,n>>;
    static constexpr size_t dimensionality = n;
    static constexpr bool is_mutable_view = iterator_traits<I>::is_mutable;
    static constexpr bool is_const_view = !is_mutable_view;
    using value_type = utils::recursive_value_type_t<nd_view<I,n>,dimensionality>;
    using reference = utils::adapt_constness<value_type&,!is_mutable_view>;
  };

  #ifdef EXPERIMENTAL_FEATURES

  template <typename val_t, size_t n>
  class range_traits<::numcpp::experimental::nd_vector<val_t,n>> {
  public:
    using iterator_type = utils::iterator_on_t<::numcpp::experimental::nd_vector<val_t,n>>;
    static constexpr size_t dimensionality = n;
    static constexpr bool is_mutable_range = iterator_traits<iterator_type>::is_mutable;
    static constexpr bool is_const_range = !is_mutable_range;
    using reference = typename iterator_traits<iterator_type>::reference;
    using value_type = typename iterator_traits<iterator_type>::value_type;
  };

  #endif
}

#endif
