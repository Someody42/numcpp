#ifndef NUMCPP_UTILS_HPP
#define NUMCPP_UTILS_HPP

#include "macros.hpp"
#include "nd_view.hpp"
#include "declarations.hpp"

#include <vector>
#include <iterator>
#include <tuple>
#include <type_traits>

/** Ce fichier déclare des utilitaires en tout genres
  * Franchement, vous devriez regarder ailleurs
  */

namespace numcpp {

  namespace utils {

    template <typename T>
    using add_const_t = std::conditional_t< std::is_reference_v<T>,
                                            std::conditional_t< std::is_lvalue_reference_v<T>,
                                                                std::add_lvalue_reference_t<std::add_const_t<std::remove_reference_t<T>>>,
                                                                std::add_rvalue_reference_t<std::add_const_t<std::remove_reference_t<T>>>
                                                                >,
                                            std::add_const_t<T>
                                            >;

    template <typename T>
    struct arrow_solver {
      std::remove_reference_t<T> val;
      arrow_solver(T obj) : val(obj) {}
      T* operator->() {
        return &val;
      }
      operator T() {
        return val;
      }
    };

    template <typename T, size_t n>
    struct repetitive_tuple_type {
      using type = decltype(std::tuple_cat(std::declval<std::tuple<T>>(),std::declval<repetitive_tuple_type<T,n-1ul>>()));
    };

    template <typename T>
    struct repetitive_tuple_type<T,0ul> {
      using type = std::tuple<>;
    };

    template <typename range_t>
    struct dimensionality{
      static constexpr size_t value = range_traits<range_t>::dimensionality;
    };

    template <typename I, size_t n>
    struct dimensionality<nd_view<I,n>>{
      static constexpr size_t value = n;
    };

    template <typename val_t, size_t n>
    struct dimensionality<nd_vector<val_t,n>>{
      static constexpr size_t value = n;
    };

    template <typename range_t>
    inline constexpr size_t dimensionality_v = dimensionality<range_t>::value;

    template <typename range_t>
    struct iterator_on{
      using type = decltype(std::declval<range_t>().begin());
    };

    template <typename I, size_t n>
    struct iterator_on<nd_view<I,n>>{
      using type = viewified_iterator<I,n>;
    };

    template <typename val_t, size_t n>
    struct iterator_on<nd_vector<val_t,n>>{
      using type = typename std::vector<nd_vector<val_t,n-1>>::iterator;
    };

    template <typename val_t>
    struct iterator_on<nd_vector<val_t,1ul>>{
      using type = typename std::vector<val_t>::iterator;
    };

    template <typename range_t>
    using iterator_on_t = typename iterator_on<range_t>::type;

    template <typename I, size_t n>
    struct recursive_iterator_type{
      using type = typename recursive_iterator_type<iterator_on_t<typename std::iterator_traits<I>::reference>,n-1>::type;
    };

    template <typename I>
    struct recursive_iterator_type<I,0>{
      using type = I;
    };

    template <typename I, size_t n>
    using recursive_iterator_type_t = typename recursive_iterator_type<I,n>::type;

    template <typename range_t, size_t n>
    struct recursive_value_type{
      using type = typename iterator_traits<recursive_iterator_type_t<iterator_on_t<range_t>,n-1>>::value_type;
    };

    template <typename range_t>
    struct recursive_value_type<range_t,0ul>{
      using type = range_t;
    };

    template <typename range_t, size_t n>
    using recursive_value_type_t = typename recursive_value_type<range_t,n>::type;

    template <typename range_t, size_t n>
    struct recursive_reference{
      using type = typename iterator_traits<recursive_iterator_type_t<iterator_on_t<range_t>,n-1>>::reference;
    };

    template <typename range_t>
    struct recursive_reference<range_t,0ul>{
      using type = range_t;
    };

    template <typename range_t, size_t n>
    using recursive_reference_t = typename recursive_reference<range_t,n>::type;

    template <typename view_t, size_t levels = 1ul>
    using lower_dimensionality = recursive_reference_t<view_t,levels>;

    template <size_t view_dim, typename ...Ts>
    struct resulting_dim;

    template <size_t view_dim, typename ...Ts>
    constexpr size_t resulting_dim_v = resulting_dim<view_dim,Ts...>::value;

    template <size_t view_dim>
    struct resulting_dim<view_dim> {
      static_assert(view_dim>=0,"You are going too deep. Remember not to group together two layers of dimensionality. ");
      static constexpr size_t value = view_dim;
    };

    template <size_t view_dim,typename T, typename ...Ts>
    struct resulting_dim<view_dim,T,Ts...> {
      static_assert(view_dim>=0,"You are going too deep. Remember not to group together two layers of dimensionality. ");
      static_assert(std::is_convertible_v<T,size_t>, "Unknown index type");
      static constexpr size_t value = resulting_dim_v<view_dim-1,Ts...>;
    };

    template <size_t view_dim,typename ...Ts>
    struct resulting_dim<view_dim,seq_bounds,Ts...> {
      static_assert(view_dim>=0,"You are going too deep. Remember not to group together two layers of dimensionality. ");
      static constexpr size_t value = 1+resulting_dim_v<view_dim-1,Ts...>;
    };

    template <size_t view_dim, typename ...Ts>
    struct resulting_dim<view_dim,std::tuple<Ts...>> {
      static constexpr size_t value = resulting_dim_v<view_dim,Ts...>;
    };

    template <typename T>
    struct remove_until_seq_bounds{
      using type = T;
      static constexpr size_t place = 0;
    };

    template <typename T, typename ...Ts>
    struct remove_until_seq_bounds<std::tuple<T,Ts...>>{
      using type = typename remove_until_seq_bounds<std::tuple<Ts...>>::type;
      static constexpr size_t place = 1ul + remove_until_seq_bounds<std::tuple<Ts...>>::place;
    };

    template <typename ...Ts>
    struct remove_until_seq_bounds<std::tuple<seq_bounds,Ts...>>{
      using type = std::tuple<size_t,Ts...>;
      static constexpr size_t place = 0;
    };

    template <typename view_t, size_t d, size_t ird, typename ...Ts>
    struct subscript_result_type_computer {
      static constexpr size_t dim = d;
      static constexpr size_t index_related_dim = ird;
      using seq_type = remove_until_seq_bounds<std::tuple<Ts...>>;
      using type = nd_view<sequence_iterator_<recursive_reference_t<view_t,seq_type::place>,typename seq_type::type>,dim>;
    };

    template <typename view_t, size_t d, typename ...Ts>
    struct subscript_result_type_computer<view_t,d,0,Ts...> {
      static constexpr size_t dim = d;
      static constexpr size_t index_related_dim = 0;
      using type = recursive_reference_t<view_t,std::tuple_size_v<std::tuple<Ts...>>>;
    };

    template <typename view_t, typename ...Ts>
    struct subscript_result_type_computer<view_t,0,0,Ts...> {
      static constexpr size_t dim = 0;
      static constexpr size_t index_related_dim = 0;
      using type = typename range_traits<view_t>::reference;
    };

    template <typename view_t, typename ...Ts>
    struct subscript_result_type {
      static constexpr size_t dim = resulting_dim_v<range_traits<view_t>::dimensionality,Ts...>;
      static constexpr size_t index_related_dim = resulting_dim_v<std::tuple_size_v<std::tuple<Ts...>>,Ts...>;
      using type = typename subscript_result_type_computer<view_t,dim,index_related_dim,Ts...>::type;

    };

    template <typename view_t, typename ...Ts>
    struct subscript_result_type<view_t,std::tuple<Ts...>> {
      static constexpr size_t dim = subscript_result_type<view_t,Ts...>::dim;
      static constexpr size_t index_related_dim = subscript_result_type<view_t,Ts...>::index_related_dim;
      using type = typename subscript_result_type<view_t,Ts...>::type;
    };

    template <typename view_t, typename ...Ts>
    using subscript_result_type_t = typename subscript_result_type<view_t,Ts...>::type;

    template <typename I, size_t d>
    struct flat_iterator_type {
      using type = flat_iterator_<I,d>;
    };

    template <typename I>
    struct flat_iterator_type<I,1> {
      using type = iterator_on_t<nd_view<I,1>>;
    };

    template <typename I,size_t n>
    using flat_iterator_type_t = typename flat_iterator_type<I,n>::type;

    template <typename T, size_t n>
    struct tuple_repeat{
      constexpr static auto function(T val){
        return std::tuple_cat(std::tuple(val),tuple_repeat<T,n-1ul>::function(val));
      }
    };

    template <typename T>
    struct tuple_repeat<T,0ul>{
      constexpr static auto function(T val){
        return std::tuple<>();
      }
    };

    template <typename range_t>
    struct view_of{
      using type = nd_view<iterator_on_t<range_t>,dimensionality_v<range_t>>;
    };

    template <typename range_t>
    using view_of_t = typename view_of<range_t>::type;

    template <typename I, size_t n>
    struct viewified_iterator_reference{
      using type = nd_view<recursive_iterator_type_t<I,1ul>,n-1ul>;
    };

    template <typename I>
    struct viewified_iterator_reference<I,1ul>{
      using type = typename std::iterator_traits<I>::reference;
    };

    template <typename I,size_t n>
    using viewified_iterator_reference_t = typename viewified_iterator_reference<I,n>::type;

    template <typename T, bool make_const>
    using adapt_constness = std::conditional_t<make_const,add_const_t<T>,T>;

    template <typename val_t, size_t n>
    struct nd_std_vector{
      using type=std::vector<typename nd_std_vector<val_t,n-1ul>::type>;
    };

    template <typename val_t>
    struct nd_std_vector<val_t,1ul>{
      using type=std::vector<val_t>;
    };

    template <typename val_t, size_t n>
    using nd_std_vector_t = typename nd_std_vector<val_t,n>::type;

    struct dummy_type{} dummy_val;

    #ifdef EXPERIMENTAL_FEATURES

    template <typename val_t, size_t n>
    struct decrease_vector_dim {
      using type = ::numcpp::experimental::nd_vector<val_t,n-1ul>;
    };

    template <typename val_t>
    struct decrease_vector_dim<val_t,1ul> {
      using type = val_t;
    };

    template <typename val_t, size_t n>
    using decrease_vector_dim_t = typename decrease_vector_dim<val_t,n>::type;

    template <typename val_t,size_t n>
    struct iterator_on<::numcpp::experimental::nd_vector<val_t,n>>{
      using type = typename std::vector<decrease_vector_dim_t<val_t,n>>::iterator;
    };

    #endif
  }
}

#endif
