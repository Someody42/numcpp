#include <iostream>
#include <algorithm>
#include <tuple>
#include <functional>
#include <typeinfo>
#include <vector>
#include <forward_list>

#include "numcpp/nd_view.hpp"
#include "numcpp/nd_vector.hpp"
#include "numcpp/arithmetic.hpp"
#include "numcpp/linalg.hpp"
#include "numcpp/algorithm.hpp"
#include "numcpp/utils.hpp"
#include "numcpp/bounds.hpp"
#include "numcpp/axis.hpp"
#include "numcpp/make_things.hpp"

namespace nc = numcpp;

int main(int argc, char const *argv[]) {

  nc::experimental::nd_vector<int,2ul> v({{0,1},
                                      {2,3}});

  std::cout<<typeid(v[nc::all]).name()<<std::endl;

  return 0;
}
