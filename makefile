#FROM WIKIPEDIA xD

# Generic GNUMakefile

# Just a snippet to stop executing under other make(1) commands
# that won't understand these lines
ifneq (,)
This makefile requires GNU Make.
endif

CPPS := main.cpp
OBJS := main.o
CC = clang++
CFLAGS = -std=c++17 -g -Wall -pedantic -fexceptions -Iinclude/
LDFLAGS =
LDLIBS = -lm -lstdc++ -lstdc++fs

all: .depend $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) $(LDFLAGS) -o main $(LDLIBS)

depend: .depend

.depend: cmd = g++ -Iinclude/ -MM -MF depend $(var); cat depend >> .depend;
.depend:
	@echo "Generating dependencies..."
	@$(foreach var, $(CPPS), $(cmd))
	@rm -f depend

-include .depend

# These are the pattern matching rules. In addition to the automatic
# variables used here, the variable $* that matches whatever % stands for
# can be useful in special cases.
%.o: %.cpp
	$(CC) $(CFLAGS) -c $< -o $@

%: %.cpp
	$(CC) $(CFLAGS) -o $@ $<

clean:
	rm -f .depend $(OBJS) main.o

.PHONY: clean depend
